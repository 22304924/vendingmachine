var readlineSync = require('readline-sync');
var credit = 0;
var a1price = 0.6;
var a2price = 0.6;
var a3price = 0.6;
var a4price = 0.6;
var b1price = 1.15;
var b2price = 0.99;
var b3price = 1;
var b4price = 1.5;

function items() {
  console.log('Your current credit is: ' + credit);
  console.log('=================================');
  console.log('[A1] Coke 330ml             $0.60 ');
  console.log('[A2] Diet Coke 330ml        $0.60');
  console.log('[A3] Pepsi 330ml            $0.60 ');
  console.log('[A4] Diet Pepsi 330ml       $0.60');
  console.log('[B1] Monster 500ml          $1.15 ');
  console.log('[B2] Rockstar 500ml         $0.99');
  console.log('[B3] Lucozade 500ml         $1.00 ');
  console.log('[B4] Red Bull 250ml         $1.50');
  console.log('=================================');
  }

function purchase() {
  var makepurchase = readlineSync.question('Please enter the product code you wish to purchase [E.G "A2"] or type "Exit" to leave. ');
  if(makepurchase === 'A1') {
    if(credit < a1price){
      console.log('=================================');
      console.log('You have insuffienct credit for this purchase.');
      console.log('Please add more credit');
      extracredit();
    } else {
      credit = credit - a1price;
      console.log('=================================');
      console.log('Thank you for your purchase!');
      console.log('Your new credit balance is: $' + credit);
      creditremaining();
    }
  } else if (makepurchase === 'A2') {
    if(credit < a2price){
      console.log('=================================');
      console.log('You have insuffienct credit for this purchase.');
      console.log('Please add more credit');
      extracredit();
    } else {
      credit = credit - a2price;
      console.log('=================================');
      console.log('Thank you for your purchase!');
      console.log('Your new credit balance is: $' + credit);
      creditremaining();
    }
  } else if (makepurchase === 'A3') {
    if(credit < a3price){
      console.log('=================================');
      console.log('You have insuffienct credit for this purchase.');
      console.log('Please add more credit');
      extracredit();
    } else {
      credit = credit - a3price;
      console.log('=================================');
      console.log('Thank you for your purchase!');
      console.log('Your new credit balance is: $' + credit);
      creditremaining();
    }
  } else if (makepurchase === 'A4') {
    if(credit < a4price){
      console.log('=================================');
      console.log('You have insuffienct credit for this purchase.');
      console.log('Please add more credit');
      extracredit();
    } else {
      credit = credit - a4price;
      console.log('=================================');
      console.log('Thank you for your purchase!');
      console.log('Your new credit balance is: $' + credit);
      creditremaining();
    }
  } else if (makepurchase === 'B1') {
    if(credit < b1price){
      console.log('=================================');
      console.log('You have insuffienct credit for this purchase.');
      console.log('Please add more credit');
      extracredit();
    } else {
      credit = credit - b1price;
      console.log('=================================');
      console.log('Thank you for your purchase!');
      console.log('Your new credit balance is: $' + credit);
      creditremaining()
    }
  } else if (makepurchase === 'B2') {
    if(credit < b2price){
      console.log('=================================');
      console.log('You have insuffienct credit for this purchase.');
      console.log('Please add more credit');
      extracredit();
    } else {
      credit = credit - b2price;
      console.log('=================================');
      console.log('Thank you for your purchase!');
      console.log('Your new credit balance is: $' + credit);
      creditremaining();
    }
  } else if (makepurchase === 'B3') {
    if(credit < b3price){
      console.log('=================================');
      console.log('You have insuffienct credit for this purchase.');
      console.log('Please add more credit');
      extracredit();
    } else {
      credit = credit - b3price;
      console.log('=================================');
      console.log('Thank you for your purchase!');
      console.log('Your new credit balance is: $' + credit);
      creditremaining();
    }
  } else if (makepurchase === 'B4') {
    if(credit < b4price){
      console.log('=================================');
      console.log('You have insuffienct credit for this purchase.');
      console.log('Please add more credit');
      extracredit();
    } else {
      credit = credit - b4price;
      console.log('=================================');
      console.log('Thank you for your purchase!');
      console.log('Your new credit balance is: $' + credit);
      creditremaining();
      }
    } else if (makepurchase === 'Exit') {
      exit();

    }
}

function extracredit() {
  var extra = readlineSync.question('Would you like to add more credit? [yes/no] ')
  if(extra === 'yes') {
    var update = readlineSync.questionFloat('Okay, how much more would you like to add? $')
    credit = credit + update;
    console.log('=================================');
    console.log('Your current credit is now: $' + credit);
    console.log('=================================');
    var again = readlineSync.question('Would you like to see our products list again? [yes/no] ')
    if(again === 'yes') {
      items();
      purchase();
    } else if(again === 'no') {
      exit();
    }
  } else if(extra === 'no') {
    exit();
  }
}

function creditremaining() {
  if (credit >= 0) {
    var anotherpurchase = readlineSync.question('You still have credit remaining. Would you like to make another purchase? [yes/no] ')

    if(anotherpurchase === 'yes'){
      items();
      purchase();
    } else if(anotherpurchase === 'no') {
      exit();
    }
  }

}

function exit() {
    var exit = readlineSync.question('Okay. Type "Exit" to obtain a refund on your current credit ');
      if(exit === 'Exit') {
        console.log('$'+ credit + ' has now been refunded. Have a nice day!')
      }
}

console.log('=================================');
console.log('=-           Welcome           -=');
console.log('=================================');
console.log('Your current credit is: $' + credit);
var topup =  readlineSync.question('Would you like to add some credit? [yes/no] ');

if(topup === 'yes') {
  console.log('=================================');
  var credit = readlineSync.questionFloat('Great!, how much would you like to add? $');
    console.log('=================================');
  console.log('Your credit is: $' + credit);
  var products = readlineSync.question('Would you like to see our products? [yes/no] ');
  if(products === 'yes') {
    items();
    purchase ();

  } else if (products === 'no') {
        exit();
  }
} else if (topup === 'no'){
  console.log('Okay, have a nice day!');
}
